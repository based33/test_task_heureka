FROM python:3.11.1 as base

WORKDIR /app

RUN pip install poetry

COPY requirements.txt /app/

RUN pip install -r requirements.txt

COPY . /app/
