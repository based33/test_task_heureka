# Cosmonaut Records App

## Description

Using this API, you can create, delete, modify, and view records of cosmonauts.

## Launch
To run the application, you will need Docker. Execute the following commands in the project's root directory:
```sh
docker build -t cosmonaut_records_app .
docker-compose up
```
> Note: `It takes time for "worker" to start, because it depends on RabbitMQ server, which is not ready immediately after launch. The "worker" will automatically restart itself until it successfully connects to the RabbitMQ server. This typically takes 10 seconds.`

 Now you can access the application at http://127.0.0.1:8000/api/cosmonauts/
 
 ## Requests
 
 ### List all cosmonauts 
 GET http://127.0.0.1:8000/api/cosmonauts/
##### Response

 ```
 [
    {
        "id": 1,
        "first_name": "Hire",
        "last_name": "Me",
        "birth_date": "1999-09-09",
        "flights_count": 10,
        "country": "Czech",
        "is_fit_for_flight": true
    },
    {
        "id": 2,
        "first_name": "Nikita",
        "last_name": "Programmer",
        "birth_date": "1995-10-10",
        "flights_count": 0,
        "country": "Czech",
        "is_fit_for_flight": false
    }
]
```


 ### List limited amount of cosmonauts 
 GET http://127.0.0.1:8000/api/cosmonauts/?limit=1
##### Response:
 ```
 {
    "count": 2,
    "next": "http://127.0.0.1:8000/api/cosmonauts/?limit=1&page=2",
    "previous": null,
    "results": [
        {
            "id": 1,
            "first_name": "Hire",
            "last_name": "Me",
            "birth_date": "1999-09-09",
            "flights_count": 10,
            "country": "Czech",
            "is_fit_for_flight": true
        }
    ]
}
 ```

 ### List special cosmonaut 
 GET http://127.0.0.1:8000/api/cosmonauts/{cosmonaut_id}
##### Response:
  ```
 {
    "id": 1,
    "first_name": "Hire",
    "last_name": "Me",
    "birth_date": "1999-09-09",
    "flights_count": 10,
    "country": "Czech",
    "is_fit_for_flight": true
}
 ```
 
 ### Create cosmonaut record
 POST http://127.0.0.1:8000/api/cosmonauts/
##### Data format:
 ```
  {
    "id": 1,
    "first_name": "Hire",
    "last_name": "Me",
    "birth_date": "1999-09-09",
    "flights_count": 10,
    "country": "Czech",
    "is_fit_for_flight": true
}
 ```
##### Response:
 ```
{
    "message": "Data received and scheduled for processing."
}
 ```

### Update cosmonaut rercord
PUT http://127.0.0.1:8000/api/cosmonauts/{cosmonaut_id}/
##### Data format
  ```
 {
    "first_name": "Hire",
    "last_name": "Me",
    "birth_date": "1976-11-19",
    "flights_count": 44,
    "country": "{UPDATED DATA}",
    "is_fit_for_flight": true
}
 ```
##### Response:
 ```
{
    "id": 1,
    "first_name": "Hire",
    "last_name": "Me",
    "birth_date": "1999-09-09",
    "flights_count": 10,
    "country": "Africa",
    "is_fit_for_flight": true
}
 ```

### Delete cosmonaut record
DELETE http://127.0.0.1:8000/api/cosmonauts/{cosmonaut_id}/
##### Response:
  ```204 status "No content" ```
 