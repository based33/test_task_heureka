from rest_framework.test import APITestCase
from rest_framework import status
from .models import Cosmonaut
from faker import Faker
from unittest.mock import patch


class CosmonautViewSetCRUDTestCase(APITestCase):
    def setUp(self):
        self.cosmonaut_data = self.create_cosmonaut_data()
        self.cosmonaut = Cosmonaut.objects.create(**self.cosmonaut_data)

    @patch("cosmonaut_records_api.views.send_to_queue")
    def test_post_cosmonaut(self, mock_send_to_queue):
        response = self.client.post("/api/cosmonauts/", self.cosmonaut_data, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, {"message": "Data received and scheduled for processing."})
        mock_send_to_queue.assert_called_once()

    def test_read_cosmonaut(self):
        response = self.client.get("/api/cosmonauts/")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)
        self.assertEqual(response.data[0]["first_name"], self.cosmonaut_data["first_name"])

    def test_update_cosmonaut(self):
        updated_data = self.create_cosmonaut_data()
        response = self.client.put(f"/api/cosmonauts/{self.cosmonaut.id}/", updated_data, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.cosmonaut.refresh_from_db()
        self.assertEqual(self.cosmonaut.first_name, updated_data["first_name"])

    def test_delete_cosmonaut(self):
        response = self.client.delete(f"/api/cosmonauts/{self.cosmonaut.id}/")
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(Cosmonaut.objects.count(), 0)

    @staticmethod
    def create_cosmonaut_data():
        fake = Faker()
        return {
            "first_name": fake.first_name(),
            "last_name": fake.last_name(),
            "birth_date": fake.date_of_birth(),
            "flights_count": fake.pyint(0, 999),
            "country": fake.country(),
            "is_fit_for_flight": fake.pybool(),
        }
