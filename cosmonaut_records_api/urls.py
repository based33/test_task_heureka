from rest_framework.routers import DefaultRouter
from .views import CosmonautViewSet

router = DefaultRouter()
router.register(r"cosmonauts", CosmonautViewSet)