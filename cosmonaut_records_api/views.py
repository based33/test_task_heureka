from rest_framework import viewsets
from rest_framework.pagination import PageNumberPagination

from cosmonaut_records.settings import QUEUE_NAME
from .models import Cosmonaut
from .serializers import CosmonautSerializer
from rest_framework.response import Response
import pika
import json


def send_to_queue(data):
    connection = pika.BlockingConnection(pika.ConnectionParameters("rabbitmq"))
    channel = connection.channel()
    channel.queue_declare(queue=QUEUE_NAME)

    channel.basic_publish(exchange="",
                          routing_key=QUEUE_NAME,
                          body=data)
    connection.close()


class CosmonautPagination(PageNumberPagination):
    page_size_query_param = "limit"


class CosmonautViewSet(viewsets.ModelViewSet):
    queryset = Cosmonaut.objects.all()
    serializer_class = CosmonautSerializer
    pagination_class = CosmonautPagination

    def create(self, request, *args, **kwargs):
        data = json.dumps(request.data)
        send_to_queue(data)
        return Response({"message": "Data received and scheduled for processing."})

