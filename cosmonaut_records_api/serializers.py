from rest_framework import serializers
from .models import Cosmonaut


class CosmonautSerializer(serializers.ModelSerializer):
    class Meta:
        model = Cosmonaut
        fields = "__all__"
