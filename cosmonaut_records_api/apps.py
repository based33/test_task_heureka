from django.apps import AppConfig


class CosmonautRecordsApiConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "cosmonaut_records_api"

    def ready(self):
        pass
