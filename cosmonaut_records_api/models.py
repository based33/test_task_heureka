from django.db import models


class Cosmonaut(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    birth_date = models.DateField()
    flights_count = models.PositiveIntegerField()
    country = models.CharField(max_length=50)
    is_fit_for_flight = models.BooleanField()

    def __str__(self):
        return f"{self.first_name} {self.last_name}"
