import json
import pika
import django
import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "cosmonaut_records.settings")
django.setup()

from cosmonaut_records.settings import QUEUE_NAME
from cosmonaut_records_api.serializers import CosmonautSerializer


def main():
    connection = pika.BlockingConnection(pika.ConnectionParameters("rabbitmq"))
    channel = connection.channel()
    channel.queue_declare(queue=QUEUE_NAME)

    def callback(ch, method, properties, body):
        data = json.loads(body)
        print(f" [x] Received {json.loads(body)}")
        serializer = CosmonautSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            print(f"Data saved: {data}")
        else:
            print(f"Invalid data: {data}")

    channel.basic_consume(queue=QUEUE_NAME,
                          auto_ack=True,
                          on_message_callback=callback)

    print(" [*] Waiting for messages. To exit press CTRL+C")
    channel.start_consuming()

if __name__ == "__main__":
    main()
